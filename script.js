// console.log("Hello World")

let trainer = {};

trainer.name = "Ash Ketchum";
trainer.age = 10;
trainer.pokemon = ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"];
trainer.friends = {};
trainer.friends.hoenn = ["May", "Max"];
trainer.friends.kanto = ["Brock", "Misty"];
trainer.talk = function(pokemon){
    console.log(pokemon + " I choose you!")
}
console.log(trainer);
console.log("Result of dot notation:");
console.log(trainer.name);
console.log("Result of square bracket notation:");
console.log(trainer["pokemon"]);
console.log("Result of talk method");
trainer.talk("Pikachu");

function Pokemon (name, level){
    this.name = name;
    this.level = level;
    this.health = level * 2;
    this.attack = level;
    this.tackle = function(target){
        console.log(this.name + " tackled " + target.name)
        console.log(target.name +" health is reduced to " + (target.health-this.attack));
        
        if((target.health-this.attack) <=0){
            target.faint();
        }
        target.health = target.health-this.attack
        console.log(target)
    }
    this.faint = function(){
        console.log(this.name + " fainted.");
    }
}

let pikachu = new Pokemon("Pikachu", 12)
console.log(pikachu);

let geodude = new Pokemon("Geodude", 8)
console.log(geodude);

let mewtwo = new Pokemon("Mewtwo", 100)
console.log(mewtwo);

geodude.tackle(pikachu);
mewtwo.tackle(geodude);